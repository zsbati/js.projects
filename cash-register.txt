/*Design a cash register drawer function 
checkCashRegister() that accepts purchase price 
as the first argument (price), payment as the 
second argument (cash), and cash-in-drawer (cid) 
as the third argument.

cid is a 2D array listing available currency.

The checkCashRegister() function should always 
return an object with a status key and a change key.

Return {status: "INSUFFICIENT_FUNDS", change: []} 
if cash-in-drawer is less than the change due, 
or if you cannot return the exact change.

Return {status: "CLOSED", change: [...]} with cash-in-drawer as the value for the key change 
if it is equal to the change due.

Otherwise, return {status: "OPEN", change: [...]}, 
with the change due in coins and bills, sorted 
in highest to lowest order, as the value of the 
change key.*/

function checkCashRegister(price, cash, cid) {
  var change = [];
  let change1 = cash - price; //changes
  let toReturn = change1; //fixed
  let toVerify = 0;
  let change2 = 0;
  let total = 0;
  let hundreds = cid[8][1];
  let twenties = cid[7][1];
  let tens = cid[6][1];
  let fives = cid[5][1];
  let ones = cid[4][1];
  let quarters = cid[3][1];
  let dimes = cid[2][1];
  let nickels = cid[1][1];
  let pennies = cid[0][1];

  for (let i=0; i<cid.length; i++){
    total += cid[i][1];
  }

  total = Math.round(100*total)/100;

  if (change1 > total){
    return {status: "INSUFFICIENT_FUNDS", change: []};
  }

  if (change1 === total){
    return {status: "CLOSED", change: cid};
  }

  if (change1 >= 100){
    let hundredsToPay = Math.floor(change1/100)*100;
    if (hundredsToPay >= hundreds){
      change.push(["ONE HUNDRED", hundreds]);
      change1 -= hundreds;
    } else {
      change.push(["ONE HUNDRED", hundredsToPay]);
      change1 -=hundredsToPay;
    }
    total -= hundreds
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= hundreds
  }

  

  if (change1 >= 20){
    let twentiesToPay = Math.floor(change1/20)*20;
    if (twentiesToPay >= twenties){
      change.push(["TWENTY", twenties]);
      change1 -= twenties;
    } else {
      change.push(["TWENTY", twentiesToPay]);
      change1 -= twentiesToPay;
    } 
    total -= twenties;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= twenties;
  } 

  if (change1 >= 10){
    let twentiesToPay = Math.floor(change1/10)*10;
    if (twentiesToPay >= tens){
      change.push(["TEN", tens]);
      change1 -= tens;
    } else {
      change.push(["TEN", tensToPay]);
      change1 -= tensToPay;
    } 
    total -= tens;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= tens;
  }

  if (change1 >= 5){
    let fivesToPay = Math.floor(change1/5)*5;
    if (fivesToPay >= fives){
      change.push(["FIVE", fives]);
      change1 -= fives;
    } else {
      change.push(["FIVE", fivesToPay]);
      change1 -= fivesToPay;
    } 
    total -= fives;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= fives;
  } 

  if (change1 >= 1){
    
    let onesToPay = Math.floor(change1);
    if (onesToPay >= ones){
      change.push(["ONE", ones]);
      change1 -= ones;
    } else {
      change.push(["ONE", onesToPay]);
      change1 -= onesToPay;
    } 
    total -= ones;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= ones;
  } 

  if (change1 >= 0.25){
    let quartersToPay = Math.floor(change1/0.25)*0.25;
    if (quartersToPay >= quarters){
      change.push(["QUARTER", quarters]);
      change1 -= quarters;
    } else {
      change.push(["QUARTER", quartersToPay]);
      change1 -= quartersToPay;
    } 
    total -= quarters;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= quarters;
  }

  if (change1 >= 0.1){
    let dimesToPay = Math.floor(change1/0.1)*0.1;
    if (dimesToPay >= dimes){
      change.push(["DIME", dimes]);
      change1 -= dimes;
    } else {
      change.push(["DIME", dimesToPay]);
      change1 -= dimesToPay;
    } 
    total -= dimes;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    } else {
    total -= dimes;
    }
  }  

  if (change1 >= 0.05){
    let nickelsToPay = Math.floor(change1/0.05)*0.05;
    if (nickelsToPay >= dimes){
      change.push(["NICKEL", nickels]);
      change1 -= nickels;
    } else {
      change.push(["NICKEL", nicelsToPay]);
      change1 -= nickelsToPay;
    } 
    total -= nickels;
    if (total < change1){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    }
  } else {
    total -= nickels;
  }

  if (change1 >= 0.01){
    let penniesToPay = Math.round(change1*100)/100;
    if (penniesToPay > pennies){
      return {status: "INSUFFICIENT_FUNDS", change: []};
    } else {
      change.push(["PENNY", penniesToPay]);
    } 
    
  } 

  for (let i=0; i<change.length; i++){
    toVerify += change[i][1];
  }

  if (toVerify !== toReturn){
    let diff = toReturn - toVerify;
    diff = Math.round(diff*100)/100;
    pennies = change[change.length-1][1] + diff;
    change.pop();
    change.push(["PENNY", pennies]);
  }

  return {status: "OPEN", change: change};
}

checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);
